# @summary PS1 Helpers
#
# PS1 Helper files used by assorted custom Puppet modules.
#
# @param ensure
#   Enumerated string denoting if the resource files should exist
#   Example: 'absent'
#
# @example
#   include ps1h
class ps1h (
  Enum['present', 'absent'] $ensure = 'present',
) {
  $system_drive = $facts['env_windows_installdir'].split('\\\\')[0]
  $ps1h_directory = "${system_drive}\\ProgramData\\${module_name}"
  if ($ensure == 'present') {
    $ps1_files = ['helpers.ps1', 'helpers-output.ps1', 'helpers-permission.ps1']
    file { $ps1h_directory:
      ensure  => directory,
    }
    $ps1_files.each | String $ps1_file | {
      file { "Place '${ps1h_directory}\\${ps1_file}'":
        ensure => 'file',
        path   => "${ps1h_directory}\\${ps1_file}",
        source => "puppet:///modules/${module_name}/${ps1_file}",
      }
    }
    file { "Place '${ps1h_directory}\\readme.txt'":
      ensure => 'file',
      path   => "${ps1h_directory}\\readme.txt",
      source => "puppet:///modules/${module_name}/readme.txt",
    }
  } else {
    file { "Remove ${ps1h_directory}" :
      ensure  => absent,
      path    => $ps1h_directory,
      recurse => true,
      force   => true,
    }
  }
}
