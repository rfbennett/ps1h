require 'spec_helper'

describe 'ps1h' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      context 'with all parameters' do
        let(:title) { 'ps1h_init' }
        let(:params) do
          {
            ensure: 'present',
          }
        end

        it { is_expected.to compile }
        it { is_expected.to compile.with_all_deps }
        it {
          is_expected.to contain_file('C:\\ProgramData\\ps1h').with(
            'ensure' => 'directory',
          )
        }
        it {
          is_expected.to contain_file("Place 'C:\\ProgramData\\ps1h\\helpers.ps1'").with(
            'ensure' => 'file',
            'path'   => 'C:\ProgramData\ps1h\helpers.ps1',
          )
        }
      end
    end
  end
end
