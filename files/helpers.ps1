<#
    .SYNOPSIS
        Helper functions used by various external scripts.
    .DESCRIPTION
        These are functions that have been split out for easier maintenance across multiple modules/scripts.
        Ensure any new functions are generic enough to be consumed by multiple Puppet modules if needed.
        Not all Puppet modules will utilize all functions.
        Functions contained within this file should start with "Invoke-Helper"
    .NOTES
        Version: 2024.04.02
#>

[CmdletBinding()]
param(
    [Parameter(Mandatory = $true)][string]$Identifier,
    [Parameter(Mandatory = $false)][boolean]$LogOutput = $true,
    [Parameter(Mandatory = $false)][string]$LogFolder = "$($env:WinDir)\Temp"
)

Function Invoke-HelperComparer {
    [OutputType([boolean])]
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory = $true)]$Value,
        [Parameter(Mandatory = $true)]$OtherValue,
        [Parameter(Mandatory = $false)][string]$Identifier = '',
        [Parameter(Mandatory = $false)][boolean]$IsPriorValid = $true,
        [Parameter(Mandatory = $false)][boolean]$LogOutput = $false
    )
    [boolean]$isSame = $true
    if ($LogOutput) {
        if ($Identifier -eq '') {
            Invoke-HelperOutput -Message "      '$($Value.ToString())' vs '$($OtherValue.ToString())'"
        }
        else {
            Invoke-HelperOutput -Message "      $($Identifier): '$($Value.ToString())' vs '$($OtherValue.ToString())'"
        }
    }
    if ($IsPriorValid) {
        if ($Value -ne $OtherValue) {
            if ($Identifier -eq '') {
                Invoke-HelperOutput -Message "The value of '$Value' does not match '$OtherValue'" -ForegroundColor 'Magenta' -Type 'Console'
            }
            else {
                Invoke-HelperOutput -Message "The value of '$Value' does not match '$OtherValue' ($Identifier)" -ForegroundColor 'Magenta' -Type 'Console'
            }
            $isSame = $false
        }
    }
    else {
        # No need to compare this one, because a prior comparison already failed
        $isSame = $false
    }
    return $isSame
}

Function Invoke-HelperExiter {
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory = $false)][string]$Message = '',
        [Parameter(Mandatory = $false)][int]$ExitCode = -1,
        [Parameter(Mandatory = $false)][ValidateSet('success', 'failure')][string]$Type = 'failure'
    )
    switch ($Type) {
        'success' { Invoke-HelperOutput -Message "$Message" -ForegroundColor 'Green' -Type 'Console' }
        'failure' { Invoke-HelperOutput -Message "$Message" -ForegroundColor 'Red' -Type 'Error' }
    }
    Invoke-HelperOutput -Message "================================================="
    Exit $ExitCode
}

Function Invoke-HelperItemCopier {
    [OutputType([Boolean])]
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory = $true)][string]$SourceItem,
        [Parameter(Mandatory = $true)][string]$Destination,
        [Parameter(Mandatory = $false)][string]$DestinationItemName = '',
        [Parameter(Mandatory = $false)][boolean]$BackupExistingDestinationItem = $false
    )
    [string]$functionName = "{0}" -f $MyInvocation.MyCommand
    Invoke-HelperOutput -Message "$functionName executing..."
    Invoke-HelperOutput -Message "   Destination: $Destination"
    Invoke-HelperOutput -Message "   SourceItem: $SourceItem"
    Invoke-HelperOutput -Message "   DestinationItemName: $DestinationItemName"
    Invoke-HelperOutput -Message "   BackupExistingDestinationItem: $BackupExistingDestinationItem"
    [boolean]$isSuccess = $false
    [boolean]$isExist = Test-Path -Path "$Item" -ErrorAction 'SilentlyContinue'
    if (!$isExist) {
        Invoke-HelperOutput -Message "Unable to locate the '$SourceItem' file" -Type 'Warning'
        return $isSuccess
    }
    [string]$sourceItemName = Split-Path -Path "$SourceItem" -Leaf -Resolve -ErrorAction 'SilentlyContinue'
    Invoke-HelperOutput -Message "   sourceItemName: $sourceItemName"
    [string]$destinationItem = "$Destination\$sourceItemName"
    if ('' -ne "$DestinationItemName") {
        $destinationItem = "$Destination\$DestinationItemName"
    }
    Invoke-HelperOutput -Message "   destinationItem: $destinationItem"
    if ($BackupExistingDestinationItem) {
        #try {
        $isExist = Test-Path -Path "$destinationItem" -ErrorAction 'SilentlyContinue'
        if ($isExist) {
            [string]$timestamp = Get-Date -format "MM-dd-yyyy.hh-mm-ss"
            Invoke-HelperOutput -Message "   Backing up '$destinationItem' as '$destinationItem.$timestamp.bak'"
            Copy-Item -Path "$destinationItem" -Destination "$destinationItem.$timestamp.bak" -Force -ErrorAction 'SilentlyContinue'
            $isExist = Test-Path -Path "$destinationItem.$timestamp.bak" -ErrorAction 'SilentlyContinue'
            if ($isExist) {
                Invoke-HelperOutput -Message '   Backup process completed successfully'
            }
            else {
                Invoke-HelperExiter -Message "   Failed to back up '$destinationItem'"
            }
        }
        else {
            Invoke-HelperOutput -Message "   No existing '$destinationItem' item found to back up"
        }
        # }
        # catch {
        #     Invoke-HelperExiter -Message "   An unexpected error occured backing up the existing '$destinationItem' item"
        # }
    }
    #try {
    [string]$timestamp = Get-Date -format "MM-dd-yyyy.hh-mm-ss"
    Invoke-HelperOutput -Message "   Copying '$SourceItem' as '$destinationItem'"
    [System.IO.FileInfo]$copiedItem = Copy-Item -Path "$SourceItem" -Destination "$destinationItem" -Force -PassThru -ErrorAction 'SilentlyContinue'
    if ($null -eq $copiedItem) {
        Invoke-HelperExiter -Message "   Failed to copy '$SourceItem'"
    }
    else {
        Invoke-HelperOutput -Message '   Copy process completed successfully'
        $isSuccess = $true
    }
    # }
    # catch {
    #     Invoke-HelperExiter -Message "   An unexpected error occured copying the '$SourceItem' item"
    # }
    Invoke-HelperOutput "$functionName execution complete"
    return $isSuccess
}

Function Invoke-HelperHashToObjectConverter {
    [OutputType([Collections.Generic.List[Object]])]
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory = $true)][string]$StringToConvert,
        [Parameter(Mandatory = $true)][string[]]$PropertyNames,
        [Parameter(Mandatory = $false)][boolean]$CleanupValues = $false,
        [Parameter(Mandatory = $false)][boolean]$ReplaceDoubleBackslashedValues = $true,
        [Parameter(Mandatory = $false)][boolean]$HashIncludesTitle = $true
    )
    [string]$functionName = "{0}" -f $MyInvocation.MyCommand
    Invoke-HelperOutput -Message "$functionName executing..."
    Invoke-HelperOutput -Message "   PropertyNames: $PropertyNames"
    Invoke-HelperOutput -Message "      Number of property names: $($PropertyNames.Count)"
    Invoke-HelperOutput -Message "   CleanupValues: $CleanupValues"
    Invoke-HelperOutput -Message "   ReplaceDoubleBackslashedValues: $ReplaceDoubleBackslashedValues"
    Invoke-HelperOutput -Message "   HashIncludesTitle: $HashIncludesTitle"
    Invoke-HelperOutput -Message "   StringToConvert: $StringToConvert"
    $StringToConvert = $StringToConvert.Trim()
    [Collections.Generic.List[Object]]$hashObjects = $null
    [string[]]$objectPropertyNames = @()
    [string[]]$propertyArray = @()
    [string]$propertyValue = ''
    [string]$pattern = ''
    [string]$propertyString = ''
    foreach ($propertyName in $PropertyNames) {
        $propertyName = $propertyName.Replace('"', '')
        $propertyName = $propertyName.Replace("'", '')
        $propertyName = $propertyName.Trim()
        $objectPropertyNames += $propertyName
    }
    if (!$HashIncludesTitle) {
        $StringToConvert = $StringToConvert.Replace('{', '"PlaceholderHashTitle"=>{')
    }
    if ($StringToConvert.StartsWith('[')) {
        $StringToConvert = $StringToConvert.Substring(1, $StringToConvert.length - 1)
    }
    if ($StringToConvert.EndsWith(']')) {
        $StringToConvert = $StringToConvert.Substring(0, $StringToConvert.length - 1)
    }
    if ($StringToConvert.StartsWith('{')) {
        $StringToConvert = $StringToConvert.Substring(1, $StringToConvert.length - 1)
    }
    if ($StringToConvert.EndsWith('}')) {
        $StringToConvert = $StringToConvert.Substring(0, $StringToConvert.length - 1)
    }
    if (!$HashIncludesTitle) {
        $StringToConvert = "$StringToConvert}"
    }
    [PSCustomObject]$hashStringObject = ConvertFrom-String -InputObject $StringToConvert -Delimiter '},'
    if ($null -eq $hashStringObject) {
        $hashStringObject = ConvertFrom-String -InputObject $StringToConvert -Delimiter '}'
    }
    [int]$hashCount = 0
    $hashStringObject.PSObject.Properties | ForEach-Object {
        $hashCount = $hashCount + 1
        [PSCustomObject]$hashObject = New-Object PSObject
        foreach ($objectPropertyName in $objectPropertyNames) {
            $hashObject | Add-Member -MemberType "NoteProperty" -Name "$objectPropertyName" -Value $null
        }
        if (($null -ne $_.Value) -and ('' -ne $_.Value)) {
            $hashArray = $_.Value -Split '{'
            [string]$hashString = $hashArray[1]
            if ($hashString.EndsWith('}')) {
                $hashString = $hashString.Substring(0, $hashString.length - 1)
            }
            Invoke-HelperOutput -Message "   hashString $($hashCount): $hashString"
            [int]$propertyCount = 0
            foreach ($objectPropertyName in $objectPropertyNames) {
                $propertyCount = $propertyCount + 1
                $propertyValue = ''
                $propertyArray = @()
                $pattern = "$objectPropertyName =>"
                if (!$hashString.Contains("$pattern")) {
                    $pattern = """$objectPropertyName"" =>"
                    if (!$hashString.Contains("$pattern")) {
                        $pattern = """$objectPropertyName""=>"
                        if (!$hashString.Contains("$pattern")) {
                            $pattern = "$objectPropertyName=>"
                            if (!$hashString.Contains("$pattern")) {
                                $pattern = ''
                            }
                        }
                    }
                }
                if ('' -ne $pattern) {
                    $propertyArray = $hashString -Split "$pattern"
                    $propertyString = $propertyArray[1].Trim()
                }
                else {
                    $propertyString = ''
                }
                if ('' -ne $propertyString) {
                    Invoke-HelperOutput -Message "      propertyString $($propertyCount): $propertyString"
                    Invoke-HelperOutput -Message "         pattern: $pattern"
                    if ($propertyString.StartsWith('[')) {
                        [string[]]$valueArray = $propertyString -Split ']'
                        $propertyValue = $valueArray[0]
                        Invoke-HelperOutput -Message "         Pre cleanup Array value: $propertyValue"
                        if ($propertyValue.StartsWith('[')) {
                            $propertyValue = $propertyValue.Substring(1, $propertyValue.length - 1)
                        }
                    }
                    else {
                        [string[]]$valueArray = $propertyString -Split ','
                        $propertyValue = $valueArray[0]
                        $propertyValue = $propertyValue.Trim()
                        if ($CleanupValues) {
                            Invoke-HelperOutput -Message "         Pre cleanup String or Integer value: $propertyValue"
                            [int]$characters = 0
                            [string]$newCharacter = ''
                            if ($propertyValue.StartsWith('"')) {
                                $characters = 1
                            }
                            if ($propertyValue.StartsWith('"\')) {
                                $characters = 2
                            }
                            if ($propertyValue.Length -gt $characters) {
                                $propertyValue = $propertyValue.Substring($characters, $propertyValue.length - $characters)
                            }
                            $characters = 0
                            if ($propertyValue.EndsWith('"')) {
                                $characters = 1
                            }
                            if ($propertyValue.EndsWith('\""')) {
                                $characters = 3
                                $newCharacter = '"'
                            }
                            if ($propertyValue.Length -gt $characters) {
                                $propertyValue = $propertyValue.Substring(0, $propertyValue.length - $characters)
                                $propertyValue = $propertyValue + $newCharacter
                            }
                        }
                    }
                    if ($ReplaceDoubleBackslashedValues) {
                        $propertyValue = $propertyValue.Replace('\\', '\')
                        $propertyValue = $propertyValue.Replace('\"', '"')
                    }
                    Invoke-HelperOutput -Message "         objectPropertyName: $objectPropertyName"
                    Invoke-HelperOutput -Message "         propertyValue: $propertyValue"
                    $hashObject."$objectPropertyName" = $propertyValue
                }
            }
            $hashObjects += $hashObject
        }
    }
    Invoke-HelperOutput -Message "$functionName execution complete"
    return $hashObjects
}

Function Invoke-HelperInitializer {
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory = $true)][string]$Identifier,
        [Parameter(Mandatory = $false)][boolean]$LogOutput = $true,
        [Parameter(Mandatory = $false)][string]$LogFolder = "$($env:WinDir)\Temp"
    )
    [string]$functionName = "{0}" -f $MyInvocation.MyCommand
    Invoke-HelperOutputInitializer -Identifier "$Identifier" -LogOutput $LogOutput -LogFolder $LogFolder
    Invoke-HelperOutput -Message "$functionName executing..."
    Invoke-HelperOutput -Message "   Identifier: $Identifier"
    Invoke-HelperOutput -Message "   LogFolder: $LogFolder"
    try {
        Invoke-HelperOutput -Message "   Operating System: $((Get-CimInstance -ClassName CIM_OperatingSystem).Caption)"
        Invoke-HelperOutput -Message "   OS Service Pack: $((Get-CimInstance -ClassName CIM_OperatingSystem).ServicePackMajorVersion)"
        Invoke-HelperOutput -Message "   PowerShell Version: $($PSVersionTable.PSVersion)"
        Invoke-HelperOutput -Message "   PowerShell Edition: $($PSVersionTable.PSEdition)"
        Invoke-HelperOutput -Message "   Computer Name: $($env:ComputerName)"
        Invoke-HelperOutput -Message "   User Domain: $($env:UserDomain)"
        Invoke-HelperOutput -Message "   If appropriate, ensure that the PowerShell console session was initiated with admin rights ('Run as administrator')" -Type 'Console'
    }
    catch {
        Invoke-HelperOutput -Message "$_"
        Invoke-HelperExiter -Message "An unexpected error occured while initializing" -Type 'failure'
    }
    Invoke-HelperOutput -Message "$functionName execution complete"
}



Get-ChildItem -Path "$PSScriptRoot\" -Include "*.ps1" -File -Recurse | ForEach-Object {
    if ("$($_.Name)" -ne 'helpers.ps1') {
        [string]$name = $_.Name.replace('.ps1', '')
        Write-Host "Sourcing $name..." -ForegroundColor 'DarkGray'
        ."$_"
    }
}
Invoke-HelperInitializer -Identifier "$($script:Identifier)" -LogOutput $script:LogOutput -LogFolder $script:LogFolder