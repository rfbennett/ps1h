<#
    .SYNOPSIS
        Helper functions related to permissions.
    .DESCRIPTION
        Functions contained within this file should start with "Invoke-HelperPermission"
    .NOTES
        Future To-Do: Shift away from SetACL.exe to pure code
        Version: 2024.03.19
#>

Function Invoke-HelperPermissionValidator {
    [OutputType([boolean])]
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory = $true)][ValidateSet('file', 'prn', 'reg', 'shr', 'srv', 'wmi')][string]$ObjectType,
        [Parameter(Mandatory = $true)][string]$Permissions
    )
    # https://helgeklein.com/setacl/documentation/command-line-version-setacl-exe/#validpermissions
    [boolean]$isValid = $false
    [string[]]$validPermissions = @()
    Switch ($ObjectType) {
        'file' { $validPermissions = @('read', 'write', 'list_folder', 'read_ex', 'change', 'full') }
        'prn' { $validPermissions = @('print', 'manage_printer', 'manage_documents', 'full') }
        'reg' { $validPermissions = @('read', 'full') }
        'shr' { $validPermissions = @('read', 'change', 'full') }
        'srv' { $validPermissions = @('read', 'start_stop', 'full') }
        'wmi' { $validPermissions = @('full', 'execute', 'remote_access', 'enable_account') }
    }
    [boolean]$isValid = $validPermissions.Contains($Permissions)
    return $isValid
}

Function Invoke-HelperPermissionSetter {
    [OutputType([boolean])]
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory = $true)][string]$AccountID,
        [Parameter(Mandatory = $true)][string]$ObjectName,
        [Parameter(Mandatory = $true)][ValidateSet('file', 'prn', 'reg', 'shr', 'srv', 'wmi')][string]$ObjectType,
        [Parameter(Mandatory = $true)][ValidateSet('Win32_Printer', 'Win32_Service')][string]$ObjectWMIClass,
        [Parameter(Mandatory = $true)][string]$Permissions,
        [Parameter(Mandatory = $false)][string]$SetACL = "$($env:ProgramFiles)\SetACL\SetACL.exe"
    )
    [string]$functionName = "{0}" -f $MyInvocation.MyCommand
    Invoke-HelperOutput -Message "$functionName executing..."
    # https://helgeklein.com/setacl/documentation/command-line-version-setacl-exe
    Invoke-HelperOutput -Message "   AccountID: $AccountID"
    Invoke-HelperOutput -Message "   ObjectName: $ObjectName"
    Invoke-HelperOutput -Message "   ObjectType: $ObjectType"
    Invoke-HelperOutput -Message "   ObjectWMIClass: $ObjectWMIClass"
    Invoke-HelperOutput -Message "   Permissions: $Permissions"
    Invoke-HelperOutput -Message "   SetACL: $SetACL"
    [boolean]$isSet = $false
    [boolean]$isValid = Invoke-HelperPermissionValidator -ObjectType "$ObjectType" -Permissions "$Permissions"
    if (!($isValid)) {
        Invoke-HelperExiter -Message "Invalid permisions '$Permissions' supplied for '$ObjectType'" -Type 'failure'
    }
    [boolean]$isExist = Test-Path -Path "$SetACL" -ErrorAction 'SilentlyContinue'
    if (!($isExist)) {
        Invoke-HelperExiter -Message "Failed to locate '$SetACL'" -Type 'failure'
    }
    [PSCustomObject]$wmiObject = Get-WmiObject -Class "$ObjectWMIClass" -Filter "Name='$ObjectName'" | Select-Object *
    if ($null -eq $wmiObject) {
        Invoke-HelperExiter -Message "Failed to locate an existing '$ObjectName' object" -Type 'failure'
    }
    $null = & "$SetACL" -on "$ObjectName" -ot $ObjectType -actn ace -ace "n:$AccountID;p:$Permissions"
    if ($LASTEXITCODE -ne 0) {
        Invoke-HelperOutput -Message "Ensure that '$AccountID' is a valid account" -ForegroundColor 'Yellow'
        Invoke-HelperExiter -Message "Failed to set permissions on the '$ObjectName' object for '$AccountID'" -Type 'failure'
    }
    else {
        Invoke-HelperOutput -Message "Successfully set permissions on the '$ObjectName' object for '$AccountID'" -ForegroundColor 'Green'
        $isSet = $true
    }
    Invoke-HelperOutput -Message "$functionName execution complete"
    return $isSet
}

Function Invoke-HelperPermissionTester {
    [OutputType([boolean])]
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory = $true)][string]$AccountID,
        [Parameter(Mandatory = $true)][string]$ObjectName,
        [Parameter(Mandatory = $true)][string]$Permissions,
        [Parameter(Mandatory = $true)][ValidateSet('file', 'prn', 'reg', 'shr', 'srv', 'wmi')][string]$ObjectType,
        [Parameter(Mandatory = $true)][ValidateSet('Win32_Printer', 'Win32_Service')][string]$ObjectWMIClass,
        [Parameter(Mandatory = $false)][ValidateSet('present', 'absent')][string]$Ensure = 'present',
        [Parameter(Mandatory = $false)][string]$SetACL = "$($env:ProgramFiles)\SetACL\SetACL.exe"
    )
    [string]$functionName = "{0}" -f $MyInvocation.MyCommand
    Invoke-HelperOutput -Message "$functionName executing..."
    Invoke-HelperOutput -Message "   AccountID: $AccountID"
    Invoke-HelperOutput -Message "   ObjectName: $ObjectName"
    Invoke-HelperOutput -Message "   Ensure: $Ensure"
    Invoke-HelperOutput -Message "   Permissions: $Permissions"
    Invoke-HelperOutput -Message "   SetACL: $SetACL"
    [boolean]$isSet = $false
    [boolean]$isValid = Invoke-HelperPermissionValidator -ObjectType "$ObjectType" -Permissions "$Permissions"
    if (!($isValid)) {
        Invoke-HelperExiter -Message "Invalid permisions '$Permissions' supplied for '$ObjectType'" -Type 'failure'
    }
    [System.Management.ManagementBaseObject]$wmiObject = Get-WmiObject -Class "$ObjectWMIClass" -Filter "Name='$ObjectName'" -ErrorAction 'SilentlyContinue'
    if ($null -eq $wmiObject) {
        Invoke-HelperOutput -Message "No existing '$ObjectName' object found" -ForegroundColor 'DarkRed'
    }
    else {
        [object[]]$existingPermissionsObject = & "$SetACL" -on "$ObjectName" -ot $ObjectType -actn list -lst f:csv
        [string[]]$existingPermissionEntries = $existingPermissionsObject.split(':')
        if ($Ensure -eq 'absent') {
            $isSet = $true
            foreach ($existingPermissionEntry in $existingPermissionEntries) {
                [string[]]$permissionEntry = $existingPermissionEntry.split(',')
                if ($permissionEntry[0].StartsWith($AccountID, 'CurrentCultureIgnoreCase')) {
                    Invoke-HelperOutput -Message "Existing unwanted entry found for '$AccountID'" -ForegroundColor 'Yellow'
                    $isSet = $false
                    break
                }
            }
            if ($isSet) {
                Invoke-HelperOutput -Message "No entry found for '$AccountID'" -ForegroundColor 'Green'
            }
        }
        else {
            [boolean]$isMultipleEntries = $false
            foreach ($existingPermissionEntry in $existingPermissionEntries) {
                [string[]]$permissionEntry = $existingPermissionEntry.split(',')
                if ($permissionEntry[0].StartsWith($AccountID, 'CurrentCultureIgnoreCase')) {
                    if ($permissionEntry[1] -eq $Permissions) {
                        Invoke-HelperOutput -Message "Matching entry already exists for '$AccountID'" -ForegroundColor 'Green'
                        $isSet = $true
                        break
                    }
                    else {
                        if (($ObjectType -eq 'prn') -and ($Permissions -eq 'full')) {
                            if (($permissionEntry[1] -eq 'manage_documents') -or ($permissionEntry[1] -eq 'manage_printer')) {
                                if ($isMultipleEntries) {
                                    # already found the first entry; so just needed to verify the second one
                                    $isSet = $true
                                }
                                else {
                                    $isMultipleEntries = $true
                                }
                            }
                        }
                    }
                }
            }
            if (!$isSet) {
                Invoke-HelperOutput -Message "No matching entry found for '$AccountID'" -ForegroundColor 'Yellow'
            }
        }
    }
    Invoke-HelperOutput -Message "$functionName execution complete"
    return $isSet
}

Function Invoke-HelperPermissionRemover {
    [OutputType([boolean])]
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory = $true)][string]$AccountID,
        [Parameter(Mandatory = $true)][string]$ObjectName,
        [Parameter(Mandatory = $true)][ValidateSet('file', 'prn', 'reg', 'shr', 'srv', 'wmi')][string]$ObjectType,
        [Parameter(Mandatory = $false)][string]$SetACL = "$($env:ProgramFiles)\SetACL\SetACL.exe"
    )
    [string]$functionName = "{0}" -f $MyInvocation.MyCommand
    Invoke-HelperOutput -Message "$functionName executing..."
    # https://helgeklein.com/setacl/documentation/command-line-version-setacl-exe
    Invoke-HelperOutput -Message "   AccountID: $AccountID"
    Invoke-HelperOutput -Message "   ObjectName: $ObjectName"
    Invoke-HelperOutput -Message "   ObjectType: $ObjectType"
    Invoke-HelperOutput -Message "   SetACL: $SetACL"
    [boolean]$isRemoved = $false
    $null = & "$SetACL" -on "$ObjectName" -ot $ObjectType -actn trustee -trst "n1:$AccountID;ta:remtrst;w:dacl"
    if ($LASTEXITCODE -ne 0) {
        Invoke-HelperExiter -Message "Failed to remove permissions on the '$ObjectName' object for '$AccountID'" -Type 'failure'
    }
    else {
        Invoke-HelperOutput -Message "Successfully removed permissions on the '$ObjectName' object for '$AccountID'" -ForegroundColor 'Green'
        $isRemoved = $true
    }
    Invoke-HelperOutput -Message "$functionName execution complete"
    return $isRemoved
}
