<#
    .SYNOPSIS
        Helper functions related to output.
     .DESCRIPTION
        Functions contained within this file should start with "Invoke-HelperOutput"
    .NOTES
        Version: 2024.03.19
#>

[string]$script:HelperOutputFile = ''
[boolean]$script:HelperOutputEnabled = $true
[boolean]$script:HelperOutputOverride = $false

Function Invoke-HelperOutput {
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory = $false)][AllowEmptyString()][string]$Message = '',
        [Parameter(Mandatory = $false)][ValidateSet('Console', 'Debug', 'Warning', 'Error', 'Info')][string]$Type = 'Debug',
        [Parameter(Mandatory = $false)][ValidateSet('Black', 'DarkBlue', 'DarkGreen', 'DarkCyan', 'DarkRed', 'DarkMagenta', 'DarkYellow', 'Gray', 'DarkGray', 'Blue', 'Green', 'Cyan', 'Red', 'Magenta', 'Yellow', 'White')][string]$ForegroundColor = 'DarkGray'
    )
    if ($script:HelperOutputOverride) {
        if ($Message -ne '') {
            Write-Host "$Message" -ForegroundColor "$ForegroundColor"
        }
    }
    else {
        if ($script:HelperOutputEnabled) {
            if (($Message -ne '') -and (($Type -eq 'Console') -or ($Type -eq 'Error'))) {
                Write-Host "$Message" -ForegroundColor "$ForegroundColor"
            }
        }
    }
    if ('' -ne $script:HelperOutputFile) {
        [boolean]$isExist = Test-Path -Path "$($script:HelperOutputFile)" -ErrorAction 'SilentlyContinue'
        if ($isExist) {
            [string]$user = [System.Security.Principal.WindowsIdentity]::GetCurrent().Name
            [string]$nowDate = Get-Date -format "yyyy-MM-dd, HH:mm:ss"
            "$nowDate, $user, $Type, $Message" | Add-Content -Path "$($script:HelperOutputFile)"
        }
        else {
            Write-Warning "Unable to locate the required '$($script:HelperOutputFile)' log file"
        }
    }
}

Function Invoke-HelperOutputInitializer {
    [CmdletBinding()]
    Param (
        [Parameter(Mandatory = $false)][string]$Identifier = "puppet_ps_helper",
        [Parameter(Mandatory = $false)][string]$LogFolder = "$($env:WinDir)\Temp",
        [Parameter(Mandatory = $false)][boolean]$LogOutput = $true,
        [Parameter(Mandatory = $false)][Int64]$MaxLogFileSize = 5 #MBs
    )
    if ($LogOutput) {
        [boolean]$isExist = Test-Path -Path "$LogFolder"  -PathType 'Container' -ErrorAction 'SilentlyContinue'
        if (!$isExist) {
            Write-Host "   Creating the '$LogFolder' folder" -ForegroundColor 'DarkGray'
            $null = New-Item -ItemType Directory -Path "$LogFolder"
        }
    }
    [string]$fileName = "$($Identifier)_debug.log"
    $script:HelperOutputFile = "$LogFolder\$fileName"
    $script:HelperOutputEnabled = $LogOutput
    [string]$debugEnvironmentVariable = [System.Environment]::GetEnvironmentVariable('PUPPET_PS_HELPER_DEBUG', [System.EnvironmentVariableTarget]::Machine)
    if ('True' -ne $debugEnvironmentVariable) {
        Write-Host "   To enable debug mode, create a SYSTEM environment variable named 'PUPPET_PS_HELPER_DEBUG' and set the value to 'True" -ForegroundColor 'DarkGray'
        if ($script:HelperOutputEnabled -ne $true) {
            return
        }
    }
    else {
        Write-Host "   Debug mode enabled for PS1 - you may review the contents of '$script:HelperOutputFile'" -ForegroundColor 'DarkGray'
    }
    [string]$nowDate = Get-Date -format "yyyy-MM-dd, HH:mm:ss"
    [boolean]$isExist = Test-Path -Path "$script:HelperOutputFile" -ErrorAction 'SilentlyContinue'
    if ($isExist) {
        [int]$fileSize = [System.Math]::Round(((($script:HelperOutputFile).length) / 1MB), 2)
        if ($fileSize -gt $MaxLogFileSize) {
            Write-Host '   Removing ovesized log file' -ForegroundColor 'DarkGray'
            Remove-Item -Path "$script:HelperOutputFile" -Force -ErrorAction 'SilentlyContinue'
            $isExist = Test-Path -Path "$script:HelperOutputFile" -ErrorAction 'SilentlyContinue'
            if ($isExist) {
                Write-Warning "Failed to remove '$script:HelperOutputFile'"
            }
            $null = New-Item -Path "$LogFolder" -Name "$fileName" -ItemType "File" -Value "$nowDate, $($env:UserName), Initialize $Identifier log file`r"
        }
    }
    else {
        $null = New-Item -Path "$LogFolder" -Name "$fileName" -ItemType "File" -Value "$nowDate, $($env:UserName), Initialize $Identifier log file`r"
    }
    return
}
