These PowerShell files may be utilized by assorted custom, Windows-based Puppet modules.
They were created to prevent the need to maintain the exact same code within multiple modules/scripts.
While a longer-term goal may be to phase them out in favor of Ruby-based code, 
the reality is that there is a need to "get work done" in the meantime.

Please note that these files may also be actively managed by the 'ps1h' Puppet module.