# Change Log
All notable changes to this project will be documented here.

## Release 0.1.8
- Add 'ensure' parameter to allow for easy mass cleanup
- Deprecating in favor of baking PS1 helper files into external modules (using a central repo to maintain authoritative source)

## Release 0.1.7
- Add Invoke-HelperItemCopier function to assist with item copying with backups

## Release 0.1.6
- Add LogFolder parameter to PS1 scripts to allow custom path for log file
-   For example: . "$ps1Helper" -Identifier "example_module" -LogOutput $logOutput -LogFolder "C:\Test\Logs"

## Release 0.1.5
- Add more logging to Invoke-HelperComparer PS1 function

## Release 0.1.4
- Adjust Invoke-HelperHashToObjectConverter to a more flexible format
- Add note in ReadMe about PUPPET_PS_HELPER_DEBUG environment variable

## Release 0.1.3
- Fix ReadMe formatting

## Release 0.1.2
- Adjust helpers.ps1 Invoke-HelperExiter function to use Switch statement
- Adjust helpers-output.ps1 output to better leverage 'Error' Type
- Add more intel to Usage example within ReadMe
- Remove redundant parentheses in example templates

## Release 0.1.1
- Initial release