# PS1 Helpers
PS1 Helper files used by assorted custom Puppet modules for use with Microsoft Windows.  


## Table of Contents
1. [Description](#description)
2. [Usage - Configuration options and additional functionality](#usage)
3. [Limitations - OS compatibility, etc.](#limitations)
4. [Development - Guide for contributing to the module](#development)


## Description
This module assists with centralizing certain PowerShell functions that are utilized by other Windows-based Puppet modules. It removes the need to maintain the exact same code within multiple locations/modules.  


## Usage
To use this module you simply need to 'Contain' or 'Include' it.  
Review the included templates for examples on how to consume the PS1 files  
An example of leveraging PowerShell .EPP templates:  
```
  $log_output = true
  $some_other_param = 'C:\\Windows\\notepad.exe'
  $account_password = Sensitive('MockupOfAnEYAMLClassParameter')
  $epp_param_hash = {
    'account_name' => '.\\name_that_gets_passed_in_directly',
  }
  if ($account_password.unwrap != '') {
    exec { "Executing an example PS1 template for ${module_name}":
      provider  => powershell,
      command   => epp("${module_name}/_example_command.ps1.epp", $epp_param_hash),
      onlyif    => epp("${module_name}/_example_onlyif.ps1.epp", $epp_param_hash),
      logoutput => $log_output,
    }
  }
```
Remember, you can wrap the entire command within Sensitive if you need to reduce potential leaks:  
`command => Sensitive(epp("${module_name}/_example_command.ps1.epp", $epp_param_hash)),`
`onlyif  => Sensitive(epp("${module_name}/_example_onlyif.ps1.epp", $epp_param_hash)),`

To force log output, create a SYSTEM environment variable named 'PUPPET_PS_HELPER_DEBUG' and set its value to 'True'.  


## Limitations
1. This modules uses the following to generate the contents of the REFERENCE.md file:  
  * `puppet strings generate --format markdown --out REFERENCE.md`  
2. This module has been evaluated against the following (as determined using $PSVersionTable):
  * Microsoft Windows Server 2022 (running PowerShell 5.1.20348.2227)
  * Microsoft Windows Server 2019 (running PowerShell 5.1.17763.5458)
  * Microsoft Windows Server 2016 (running PowerShell 5.1.14393.6343)
  * Microsoft Windows 11 (running PowerShell 5.1.22621.2506)


## Development
Feedback and ideas are always welcome - please contact an Author (listed in metadata.json) to discuss your input, or feel free to simply [open an Issue](https://gitlab.com/rfbennett/ps1h/-/issues).

Command to apply this module locally (and try it out):  
`puppet apply --modulepath="<PathToModuleParentFolders>" --execute "include <ModuleName>" --environment "<EnvironmentName>" --no-splay --verbose --debug`

For example: `puppet apply --modulepath="C:\ProgramData\PuppetLabs\code\environments\production\modules;c:\projects\forge" --execute "include ps1h" --environment "production" --no-splay`